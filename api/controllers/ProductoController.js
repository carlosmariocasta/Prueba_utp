/**
 * ProductoController
 *
 * @description :: Server-side logic for managing productoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    
    index: function (req,res){
        console.log('entra index');
        return res.view('vista', {title: 'services ONLIMO :: ', layout : 'layout'});
    },
    
    guardar: function(req,res){
        console.log('va a guardar');
        Marca.guardar(req.param('nombre'), req.param('marca'), function(err, data){
            if(err)
                return res.send(400,{message: "Error realizando adición de producto"});
            
            return res.send(200,data);
        });
    },
    
    getdata: function(req, res){
        console.log('get data');
        Marca.getdata(function(err, data){
            if(err)
                return res.send(400,{message: "Error obteniendo listado de productos"});
            for (var i in data){
                console.log(data[i].id);
                var id = '';
                id = data[i].id;
                data[i]['editar'] = '<button class="btn btn-default editar" rel="'+data[i].nombre+'_'+data[i].marca+'_'+id+'" type="button"><span class="icon-pencil"></spam></button>';  
            }
            return res.send(200,data);
        });
    },
    
    editar: function(req, res){
        console.log('va a editar');
        Marca.editar(req.param('nombre'), req.param('marca'), req.param('id'), function(err, data){
            if(err)
                return res.send(400,{message: "Error realizando edición de producto"});
            
            return res.send(200,data);
        });
    }
};

