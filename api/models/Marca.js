/**
 * Marca.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  getdata: function(next){
        console.log('entra getdata');
      
        this.query(' SELECT p.id, p.nombre, p.marca FROM "public".producto p ORDER BY p.id;', function(err,result){
            if(err)
                return next(err,null);
            return next(null,result.rows);
        });
    },
    
    editar: function(nombre, marca, id, next){
        console.log('entra editar');
        this.query(' UPDATE "public".producto \
                        SET nombre= \''+nombre+'\', \
                        marca=\''+marca+'\'\
                        WHERE id='+id+'::INTEGER RETURNING *;', function(err,result){
            if(err)
                return next(err,null);
            return next(null,result.rows);
        });
    },
    
    guardar: function(nombre, marca, next){

        this.query('INSERT INTO "public".producto (nombre, marca) \
                    VALUES (\''+nombre+'\', \''+marca+'\') RETURNING *;', function(err,result){
            if(err)
                return next(err,null);
            return next(null,result.rows);
        });
    }
};

